package org.whaleythom.plum.vm;

import org.whaleythom.plum.vm.memory.UIMemory;

import javax.swing.*;
import java.awt.*;

public class UIMain extends JFrame {
    public UIMain() {
        UIMemory program = new UIMemory("Program");
        UIMemory data = new UIMemory("Data");
        PlumVM vm = new PlumVM(new PlumInstructionSet(), program, data, new DefaultDisplay());

        program.write(new byte[] {
                (byte)1, // LD_A_IMM,
                (byte)0, // 0x0,
                (byte)10, // PUSH,
                (byte)1, // LD_A_IMM,
                (byte)0, // 0x0,
                (byte)10, // PUSH,
                (byte)29, // RELATIVE_JUMP,
                (byte)25, // 0x19,
                (byte)11, // POP,
                (byte)20, // SWAP_A_B,
                (byte)11, // POP,
                (byte)21, // SWAP_A_C,
                (byte)11, // POP,
                (byte)21, // SWAP_A_C,
                (byte)10, // PUSH,
                (byte)5, // ADD,
                (byte)21, // SWAP_A_C,
                (byte)11, // POP,
                (byte)20, // SWAP_A_B,
                (byte)11, // POP,
                (byte)21, // SWAP_A_C,
                (byte)10, // PUSH,
                (byte)32, // RELATIVE_JUMP_IF_OVERFLOW,
                (byte)3, // 0x3,
                (byte)29, // RELATIVE_JUMP,
                (byte)4, // 0x4,
                (byte)20, // SWAP_A_B,
                (byte)24, // INC,
                (byte)20, // SWAP_A_B,
                (byte)5, // ADD,
                (byte)10, // PUSH,
                (byte)9, // RETURN,
                (byte)30, // RELATIVE_CALL,
                (byte)-25, // 0xe7
        }, vm.getPc());

//        vm.displayInfo();
        JButton next = new JButton("Next");
        JLabel pc = new JLabel(String.format("PC: 0x%x", vm.getPc()));
        JLabel sp = new JLabel(String.format("SP: 0x%x", vm.getSp()));
        JLabel status = new JLabel("S: " + PlumInstructionSet.getReadableStatus(vm));
        JLabel nextInstruction = new JLabel("Next: " + ((PlumInstructionSet) vm.getInstructionSet()).nextByteOpName(vm));
        JLabel registerA = new JLabel(String.format("A: 0x%x", vm.readData(PlumInstructionSet.REG_A)));
        JLabel registerB = new JLabel(String.format("B: 0x%x", vm.readData(PlumInstructionSet.REG_B)));
        JLabel registerC = new JLabel(String.format("C: 0x%x", vm.readData(PlumInstructionSet.REG_C)));
        JLabel stack = new JLabel(String.format("Stack: 0x%x", vm.readData(vm.getSp())));
        program.updateAccessed(vm.getPc(), 0b100);
        data.updateAccessed(vm.getSp(), 0b100);
        next.addActionListener(e -> {
            program.clearAccessed(vm.getPc(), 0b100);
            data.clearAccessed(vm.getSp(), 0b100);
            vm.clock();
            program.updateAccessed(vm.getPc(), 0b100);
            data.updateAccessed(vm.getSp(), 0b100);
//            vm.displayInfo();
            pc.setText(String.format("PC: 0x%x", vm.getPc()));
            sp.setText(String.format("SP: 0x%x", vm.getSp()));
            status.setText("S: " + PlumInstructionSet.getReadableStatus(vm));
            nextInstruction.setText("Next: " + ((PlumInstructionSet) vm.getInstructionSet()).nextByteOpName(vm));
            registerA.setText(String.format("A: 0x%x", vm.readData(PlumInstructionSet.REG_A)));
            registerB.setText(String.format("B: 0x%x", vm.readData(PlumInstructionSet.REG_B)));
            registerC.setText(String.format("C: 0x%x", vm.readData(PlumInstructionSet.REG_C)));
            stack.setText(String.format("Stack: 0x%x", vm.readData(vm.getSp() + 1)));
            program.redraw();
            data.redraw();
        });

        this.setLayout(new GridLayout(3, 3));
        this.add(next);
        this.add(pc);
        this.add(sp);
        this.add(status);
        this.add(nextInstruction);
        this.add(stack);
        this.add(registerA);
        this.add(registerB);
        this.add(registerC);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(UIMain::new);
    }
}
