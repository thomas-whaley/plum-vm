package org.whaleythom.plum.vm.memory;

public class ArrayMemory implements Memory {
    private final byte[] memory = new byte[0xffff + 1];

    @Override
    public int size() {
        return memory.length;
    }

    @Override
    public void rawWrite(byte data, int index) {
        memory[index] = data;
    }

    @Override
    public byte rawRead(int index) {
        return memory[index];
    }
}
