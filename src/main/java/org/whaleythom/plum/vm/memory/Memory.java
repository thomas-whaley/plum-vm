package org.whaleythom.plum.vm.memory;

public interface Memory {
    int size();

    default int maxIndex() {
        return size() - 1;
    }

    default boolean outOfBounds(int index) {
        return index < 0 || index > maxIndex();
    }

    default boolean outOfBounds(int index, int size) {
        if (size < 0) {
            throw new IllegalArgumentException("Invalid size " + size);
        }
        return outOfBounds(index) || outOfBounds(index + size - 1);
    }

    /**
     * Write to the memory. Calls to rawWrite. This is useful for callbacks and custom overrides (like updating a UI).
     * @param data Data to write into memory
     * @param index Index in the memory to write to
     */
    default void write(byte data, int index) {
        rawWrite(data, index);
    }

    /**
     * Write to memory. This is the base method that should be implemented before write. The reasoning behind this is
     * if you just need to write to the memory, without callbacks to UI, or other custom codes.
     * @param data Data to write into memory
     * @param index Index in the memory to write to
     */
    void rawWrite(byte data, int index);

    default void write(byte[] data, int startIndex) {
        if (outOfBounds(startIndex, data.length)) {
            throw new IllegalArgumentException(String.format("Cannot write segment of bytes 0x%x - 0x%x. Max index 0x%x",
                    startIndex, startIndex + data.length, maxIndex()));
        }
        for (int i = 0; i < data.length; i++) {
            write(data[i], startIndex + i);
        }
    }

    default void writeWord(int data, int index) {
        if (outOfBounds(index, 2)) {
            throw new IllegalArgumentException(String.format("Cannot write word (2 bytes) 0x%x - 0x%x. Max index 0x%x", index, index+1, maxIndex()));
        }
        write((byte) (data & 0xff), index);
        write((byte) ((data & 0xff00) >> 8), index + 1);
    }

    default byte read(int index) {
        if (outOfBounds(index)) {
            throw new IllegalArgumentException(String.format("Cannot read byte at 0x%x. Max index 0x%x", index, maxIndex()));
        }
        return rawRead(index);
    }

    byte rawRead(int index);

    default byte[] read(int startIndex, int size) {
        if (outOfBounds(startIndex, size)) {
            throw new IllegalArgumentException(String.format("Cannot read segment of bytes 0x%x - 0x%x. Max index 0x%x",
                    startIndex, startIndex + size, maxIndex()));
        }
        byte[] out = new byte[size];
        for (int i = 0; i < size; i++) {
            out[i] = read(startIndex + i);
        }
        return out;
    }

    default int readWord(int index) {
        if (outOfBounds(index, 2)) {
            throw new IllegalArgumentException(String.format("Cannot read word (2 bytes) 0x%x - 0x%x. Max index 0x%x", index, index+1, maxIndex()));
        }
        int lsb = Byte.toUnsignedInt(read(index));
        int msb = Byte.toUnsignedInt(read(index + 1));
        return (msb << 8) | lsb;
    }
}
