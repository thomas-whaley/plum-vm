package org.whaleythom.plum.vm.memory;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class UIMemory extends ArrayMemory {
    private final JTable table;
    private final AccessColumnCellRenderer tableCellRenderer;
    private static final int numCols = 64;

    public void redraw() {
        table.updateUI();
    }

    public UIMemory(String name) {
        table = new JTable(size() / numCols, numCols);
        tableCellRenderer = new AccessColumnCellRenderer(size() / numCols, numCols);

        for (int i = 0; i < numCols; i ++) {
            table.getTableHeader().getColumnModel().getColumn(i).setHeaderValue(String.format("%x", i));
            table.getColumnModel().getColumn(i).setCellRenderer(tableCellRenderer);
        }

        for (int i = 0; i < size(); i++) {
            table.setValueAt("0", i / numCols, i % numCols);
        }

        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame();
            frame.add(new JScrollPane(table));
            frame.setTitle(name);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        });
    }

    private static class AccessColumnCellRenderer extends DefaultTableCellRenderer {
        private final int[][] accessed;

        public AccessColumnCellRenderer(int rows, int columns) {
            accessed = new int[rows][columns];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j ++) {
                    accessed[i][j] = 0;
                }
            }
        }
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            l.setPreferredSize(new Dimension(10, 5));
            l.setBackground(Color.WHITE);
            l.setForeground(Color.BLACK);
            if ((accessed[row][column] & 0b1) != 0) {
                l.setBackground(Color.ORANGE);
            }
            if ((accessed[row][column] & 0b10) != 0) {
                l.setForeground(Color.MAGENTA);
            }
            if ((accessed[row][column] & 0b100) != 0) {
                l.setBackground(Color.BLUE);
            }
            if ((accessed[row][column] & 0b1000) != 0) {
                l.setBackground(Color.RED);
            }
            return l;
        }
    }

    public void updateAccessed(int index, int val) {
        tableCellRenderer.accessed[index / numCols][index % numCols] |= val;
    }
    public void clearAccessed(int index, int val) {
        tableCellRenderer.accessed[index / numCols][index % numCols] &= ~val;
    }


    @Override
    public void write(byte data, int index) {
        table.setValueAt(String.format("%x", data), index / numCols, index % numCols);
        tableCellRenderer.accessed[index / numCols][index % numCols] |= 0b1;
        super.write(data, index);
    }

    @Override
    public byte read(int index) {
        tableCellRenderer.accessed[index / numCols][index % numCols] |= 0b10;
        return super.read(index);
    }
}
