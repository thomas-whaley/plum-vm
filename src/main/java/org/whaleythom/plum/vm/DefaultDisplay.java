package org.whaleythom.plum.vm;

import org.whaleythom.plum.vm.memory.Memory;

import javax.swing.*;
import java.awt.*;

public class DefaultDisplay implements Display {
    private static int PIXEL_SIZE = 5;
    private class DisplayWindow extends JPanel {
        public DisplayWindow() {
            setPreferredSize(new Dimension(displayWidth() * PIXEL_SIZE, displayHeight() * PIXEL_SIZE));
        }
    }

    private final DisplayWindow window;

    public DefaultDisplay() {
        JFrame frame = new JFrame();
        window = new DisplayWindow();
        frame.setContentPane(window);
        frame.pack();
        frame.setPreferredSize(new Dimension(displayWidth() * PIXEL_SIZE, displayHeight() * PIXEL_SIZE));
        frame.setTitle("PlumVM Video Emulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    @Override
    public void update(PlumVM vm) {
        Memory data = vm.getDataMemory();
        byte[] videoData = data.read(videoMemoryStart(vm), requiredBytes());
        Graphics g = window.getGraphics();
        for (int i = 0; i < videoData.length; i ++) {
            byte pixel = videoData[i];
            // 00000000
            // rrggbbaa
            int red = pixel & 0b11;
            int green = (pixel & 0b1100) >> 2;
            int blue =  (pixel & 0b110000) >> 4;
            int alpha = (pixel & 0b11000000) >> 6;
            g.setColor(new Color(red * 85, green * 85, blue * 85));
            int x = i % displayWidth();
            int y = i / displayHeight();
            g.fillRect(x * PIXEL_SIZE, y * PIXEL_SIZE, PIXEL_SIZE, PIXEL_SIZE);
        }
    }

    @Override
    public int displayWidth() {
        return 80;
    }

    @Override
    public int displayHeight() {
        return 80;
    }


    public static void main(String[] args) {

//        SwingUtilities.invokeLater(() -> new Video(new PlumVM(new PlumInstructionSet(), new ArrayMemory(), new ArrayMemory())));
    }
}
