package org.whaleythom.plum.vm;

import org.whaleythom.plum.vm.memory.UIMemory;

public class Main {
    public static void main(String[] args) {
        InstructionSet instructionSet = new PlumInstructionSet();
        PlumVM vm = new PlumVM(instructionSet, new UIMemory("Program"), new UIMemory("Data"), new DefaultDisplay());
        vm.setPc(0xbeef);

        vm.getProgramMemory().write(PlumInstructionSet.OP_RELATIVE_JUMP, vm.getPc());
        vm.getProgramMemory().write((byte) -2, vm.getPc() + 1);
        vm.displayInfo();
        vm.clock();
        vm.displayInfo();
        vm.clock();
        vm.displayInfo();
    }
}