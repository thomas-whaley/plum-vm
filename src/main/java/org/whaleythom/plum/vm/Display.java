package org.whaleythom.plum.vm;

public interface Display {
    void update(PlumVM plumVM);

    int displayWidth();

    int displayHeight();

    default int requiredBytes() {
        return displayHeight() * displayWidth();
    }

    default int videoMemoryStart(PlumVM plumVM) {
        return plumVM.getDataMemory().maxIndex() - requiredBytes();
    }
}
