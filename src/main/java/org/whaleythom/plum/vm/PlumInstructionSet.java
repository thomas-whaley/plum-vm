package org.whaleythom.plum.vm;

import java.util.Optional;

public class PlumInstructionSet implements InstructionSet {
    public static final byte REG_A = 0;
    public static final byte REG_B = 1;
    public static final byte REG_C = 2;
    public static final byte REG_STATUS = 3;
    public static final byte STATUS_MASK_CARRY = 0b1;
    public static final byte STATUS_MASK_NEGATIVE = 0b10;
    public static final byte STATUS_MASK_ZERO = 0b100;
    public static final byte STATUS_MASK_OVERFLOW = 0b1000;
    public static final byte STATUS_MASK_INTERRUPT = 0b10000;

    public static final byte OP_LOAD_A = 1;
    public static final byte OP_LOAD_B = 2;
    public static final byte OP_LOAD_C = 3;
    public static final byte OP_LOAD_STATUS = 4;
    public static final byte OP_ADD = 5;
    public static final byte OP_SUB = 6;
    public static final byte OP_JUMP = 7;
    public static final byte OP_CALL = 8;
    public static final byte OP_RETURN = 9;
    public static final byte OP_PUSH = 10;
    public static final byte OP_POP = 11;
    public static final byte OP_WRITE_A = 12;
    public static final byte OP_WRITE_B = 13;
    public static final byte OP_WRITE_C = 14;
    public static final byte OP_WRITE_STATUS = 15;
    public static final byte OP_READ_A = 16;
    public static final byte OP_READ_B = 17;
    public static final byte OP_READ_C = 18;
    public static final byte OP_READ_STATUS = 19;
    public static final byte OP_SWAP_A_B = 20;
    public static final byte OP_SWAP_A_C = 21;
    public static final byte OP_SWAP_A_STATUS = 22;
    public static final byte OP_CMP = 23;
    public static final byte OP_INC = 24;
    public static final byte OP_AND = 25;
    public static final byte OP_OR = 26;
    public static final byte OP_XOR = 27;
    public static final byte OP_NOT = 28;
    public static final byte OP_RELATIVE_JUMP = 29;
    public static final byte OP_RELATIVE_CALL = 30;
    public static final byte OP_RELATIVE_JUMP_IF_ZERO = 31;
    public static final byte OP_RELATIVE_JUMP_IF_OVERFLOW = 32;
    public static final byte OP_RELATIVE_JUMP_IF_NEGATIVE = 33;
    public static final byte OP_RELATIVE_JUMP_IF_CARRY = 34;
    public static final byte OP_READ_AT = 35;
    public static final byte OP_WRITE_AT = 36;
    public static final byte OP_DEC = 37;

    public static final Instruction INST_LOAD_A = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.getProgramAtPc();
            vm.writeData(a, REG_A);
        }
        @Override
        public String name() {
            return "LD_A_IMM";
        }

        @Override
        public int numArgs() { return 1; }
    };
    public static final Instruction INST_LOAD_B = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.getProgramAtPc();
            vm.writeData(a, REG_B);
        }
        @Override
        public String name() {
            return "LD_B_IMM";
        }

        @Override
        public int numArgs() { return 1; }
    };
    public static final Instruction INST_LOAD_C = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.getProgramAtPc();
            vm.writeData(a, REG_C);
        }

        @Override
        public String name() {
            return "LD_C_IMM";
        }

        @Override
        public int numArgs() { return 1; }
    };
    public static final Instruction INST_LOAD_STATUS = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.getProgramAtPc();
            vm.writeData(a, REG_STATUS);
        }

        @Override
        public String name() {
            return "LD_STATUS_IMM";
        }

        @Override
        public int numArgs() { return 1; }
    };
    public static final Instruction INST_ADD = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte regA = vm.readData(REG_A);
            byte regB = vm.readData(REG_B);
            byte regC = vm.readData(REG_C);
            byte result = (byte) (regB + regC);
            // Clear carry, overflow, negative, zero
            flags &= (byte) 0xf0;
            // Taken from AVR Instruction Set Manual DS40002198A 5.1.2
            if (((regB & regC & 0x80) |
                    (regB & ~result & 0x80) |
                    (regA & ~result & 0x80)) != 0) {
                flags |= STATUS_MASK_CARRY;
            }
            if (((regB & regC & ~result & 0x80) |
                    (~regB & ~regC & result & 0x80)) != 0) {
                flags |= STATUS_MASK_OVERFLOW;
            }
            if ((result & 0x80) != 0) {
                flags |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                flags |= STATUS_MASK_ZERO;
            }
            vm.writeData(flags, REG_STATUS);
            vm.writeData(result, REG_A);
        }

        @Override
        public String name() {
            return "ADD";
        }

        @Override
        public int numArgs() { return 0; }
    };
    public static final Instruction INST_SUB = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte regA = vm.readData(REG_A);
            byte regB = vm.readData(REG_B);
            byte regC = vm.readData(REG_C);
            byte result = (byte) (regB - regC);
            // Clear carry, overflow, negative, zero
            flags &= (byte) 0xf0;
            // Taken from AVR Instruction Set Manual DS40002198A 5.1.2
            if (((~regB & regC & 0x80) |
                    (regB & result & 0x80) |
                    (~regA & result & 0x80)) != 0) {
                flags |= STATUS_MASK_CARRY;
            }
            if (((regB & ~regC & ~result) |
                    (~regB & regC & ~result & 0x80)) != 0) {
                flags |= STATUS_MASK_OVERFLOW;
            }
            if ((result & 0x80) != 0) {
                flags |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                flags |= STATUS_MASK_ZERO;
            }
            vm.writeData(flags, REG_STATUS);
            vm.writeData(result, REG_A);
        }

        @Override
        public String name() {
            return "SUB";
        }

        @Override
        public int numArgs() { return 0; }
    };
    public static final Instruction INST_JUMP = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int jumpAddress = vm.getProgramWordAtPc();
            vm.setPc(jumpAddress);
        }

        @Override
        public String name() {
            return "JUMP_IMM";
        }

        @Override
        public int numArgs() { return 2; }
    };
    public static final Instruction INST_RELATIVE_JUMP = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte k = vm.getProgramAtPc();
            vm.setPc(vm.getPc() + k - 1);
        }

        @Override
        public String name() {
            return "JUMP_RELATIVE";
        }

        @Override
        public int numArgs() {
            return 1;
        }
    };
    public static final Instruction INST_RELATIVE_JUMP_IF_ZERO = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte k = vm.getProgramAtPc();
            if ((flags & STATUS_MASK_ZERO) != 0) {
                vm.setPc(vm.getPc() + k - 1);
            }
        }

        @Override
        public String name() {
            return "JUMP_RELATIVE_IF_ZERO";
        }

        @Override
        public int numArgs() {
            return 1;
        }
    };
    public static final Instruction INST_RELATIVE_JUMP_IF_OVERFLOW = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte k = vm.getProgramAtPc();
            if ((flags & STATUS_MASK_OVERFLOW) != 0) {
                vm.setPc(vm.getPc() + k - 1);
            }
        }

        @Override
        public String name() {
            return "JUMP_RELATIVE_IF_OVERFLOW";
        }

        @Override
        public int numArgs() {
            return 1;
        }
    };
    public static final Instruction INST_RELATIVE_JUMP_IF_NEGATIVE = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte k = vm.getProgramAtPc();
            if ((flags & STATUS_MASK_NEGATIVE) != 0) {
                vm.setPc(vm.getPc() + k - 1);
            }
        }

        @Override
        public String name() {
            return "JUMP_RELATIVE_IF_NEGATIVE";
        }

        @Override
        public int numArgs() {
            return 1;
        }
    };
    public static final Instruction INST_RELATIVE_JUMP_IF_CARRY = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte k = vm.getProgramAtPc();
            if ((flags & STATUS_MASK_CARRY) != 0) {
                vm.setPc(vm.getPc() + k - 1);
            }
        }

        @Override
        public String name() {
            return "JUMP_RELATIVE_IF_CARRY";
        }

        @Override
        public int numArgs() {
            return 1;
        }
    };
    public static final Instruction INST_CALL = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int jumpAddress = vm.getProgramWordAtPc();
            int currentAddress = vm.getPc();
            vm.pushWordSp(currentAddress);
            vm.setPc(jumpAddress);
        }

        @Override
        public String name() {
            return "CALL_IMM";
        }

        @Override
        public int numArgs() { return 2; }
    };
    public static final Instruction INST_RELATIVE_CALL = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte k = vm.getProgramAtPc();
            int currentAddress = vm.getPc();
            vm.pushWordSp(currentAddress);
            vm.setPc(vm.getPc() + k - 1);
        }

        @Override
        public String name() {
            return "RELATIVE_CALL";
        }

        @Override
        public int numArgs() {
            return 1;
        }
    };
    public static final Instruction INST_RETURN = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int jumpAddress = vm.popWordSp();
            vm.setPc(jumpAddress);
        }

        @Override
        public String name() {
            return "RETURN";
        }

        @Override
        public int numArgs() { return 0; }
    };
    public static final Instruction INST_PUSH = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte data = vm.readData(REG_A);
            vm.pushSp(data);
        }

        @Override
        public String name() {
            return "PUSH_A";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_POP = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte popped = vm.popSp();
            vm.writeData(popped, REG_A);
        }

        @Override
        public String name() {
            return "POP_A";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_WRITE_A = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_A);
            int writeAddress = vm.getProgramWordAtPc();
            vm.writeData(a, writeAddress);
        }

        @Override
        public String name() {
            return "WRITE_A";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_WRITE_B = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_B);
            int writeAddress = vm.getProgramWordAtPc();
            vm.writeData(a, writeAddress);
        }

        @Override
        public String name() {
            return "WRITE_B";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_WRITE_C = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_C);
            int writeAddress = vm.getProgramWordAtPc();
            vm.writeData(a, writeAddress);
        }

        @Override
        public String name() {
            return "WRITE_C";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_WRITE_STATUS = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_STATUS);
            int writeAddress = vm.getProgramWordAtPc();
            vm.writeData(a, writeAddress);
        }

        @Override
        public String name() {
            return "WRITE_STATUS";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_READ_A = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int readAddress = vm.getProgramWordAtPc();
            byte result = vm.readData(readAddress);
            vm.writeData(result, REG_A);
        }

        @Override
        public String name() {
            return "READ_A";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_READ_B = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int readAddress = vm.getProgramWordAtPc();
            byte result = vm.readData(readAddress);
            vm.writeData(result, REG_B);
        }

        @Override
        public String name() {
            return "READ_B";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_READ_C = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int readAddress = vm.getProgramWordAtPc();
            byte result = vm.readData(readAddress);
            vm.writeData(result, REG_C);
        }

        @Override
        public String name() {
            return "READ_C";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_READ_STATUS = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int readAddress = vm.getProgramWordAtPc();
            byte result = vm.readData(readAddress);
            vm.writeData(result, REG_STATUS);
        }

        @Override
        public String name() {
            return "READ_STATUS";
        }

        @Override
        public int numArgs() {
            return 2;
        }
    };
    public static final Instruction INST_SWAP_A_B = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_A);
            byte b = vm.readData(REG_B);
            vm.writeData(a, REG_B);
            vm.writeData(b, REG_A);
        }

        @Override
        public String name() {
            return "SWAP_A_B";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_SWAP_A_C = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_A);
            byte c = vm.readData(REG_C);
            vm.writeData(a, REG_C);
            vm.writeData(c, REG_A);
        }

        @Override
        public String name() {
            return "SWAP_A_C";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_SWAP_A_STATUS = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_A);
            byte s = vm.readData(REG_STATUS);
            vm.writeData(a, REG_STATUS);
            vm.writeData(s, REG_A);
        }

        @Override
        public String name() {
            return "SWAP_A_STATUS";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_CMP = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte flags = vm.readData(REG_STATUS);
            byte a = vm.readData(REG_A);
            byte b = vm.readData(REG_B);
            byte c = vm.readData(REG_C);
            int result = Byte.toUnsignedInt(b) - Byte.toUnsignedInt(c);
            flags &= (byte) 0xf0;
            if (((~b & c & 0x80) |
                    (b & result & 0x80) |
                    (~a & result & 0x80)) != 0) {
                flags |= STATUS_MASK_CARRY;
            }
            if (((b & ~c & ~result) |
                    (~b & c & ~result & 0x80)) != 0) {
                flags |= STATUS_MASK_OVERFLOW;
            }
            if ((result & 0x80) != 0) {
                flags |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                flags |= STATUS_MASK_ZERO;
            }
            vm.writeData(flags, REG_STATUS);
        }

        @Override
        public String name() {
            return "CMP";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_INC = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte regA = vm.readData(REG_A);
            byte result = (byte) (Byte.toUnsignedInt(regA) + 1);
            byte negResult = (byte) ~result;
            byte status = vm.readData(REG_STATUS);
            status &= (byte) 0xf0;
            if ((regA & 0b10000000) != 0 &&
                    (regA & 0b1000000) != 0 &&
                    (regA & 0b100000) != 0 &&
                    (regA & 0b10000) != 0 &&
                    (regA & 0b1000) != 0 &&
                    (regA & 0b100) != 0 &&
                    (regA & 0b10) != 0 &&
                    (regA & 0b1) != 0) {
                status |= STATUS_MASK_OVERFLOW;
            }
            if ((result & 0x80) != 0) {
                status |= STATUS_MASK_NEGATIVE;
            }
            if ((negResult & 0b10000000) != 0 &&
                    (negResult & 0b1000000) != 0 &&
                    (negResult & 0b100000) != 0 &&
                    (negResult & 0b10000) != 0 &&
                    (negResult & 0b1000) != 0 &&
                    (negResult & 0b100) != 0 &&
                    (negResult & 0b10) != 0 &&
                    (negResult & 0b1) != 0) {
                status |= STATUS_MASK_ZERO;
            }
            vm.writeData(status, REG_STATUS);
            vm.writeData(result, REG_A);
        }

        @Override
        public String name() {
            return "INC";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_DEC = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte regA = vm.readData(REG_A);
            byte result = (byte) (Byte.toUnsignedInt(regA) - 1);
            byte negResult = (byte) ~result;
            byte status = vm.readData(REG_STATUS);
            status &= (byte) 0xf0;
            if ((~regA & 0b10000000) != 0 &&
                    (~regA & 0b1000000) != 0 &&
                    (~regA & 0b100000) != 0 &&
                    (~regA & 0b10000) != 0 &&
                    (~regA & 0b1000) != 0 &&
                    (~regA & 0b100) != 0 &&
                    (~regA & 0b10) != 0 &&
                    (~regA & 0b1) != 0) {
                status |= STATUS_MASK_OVERFLOW;
            }
            if ((result & 0x80) != 0) {
                status |= STATUS_MASK_NEGATIVE;
            }
            if ((negResult & 0b10000000) != 0 &&
                    (negResult & 0b1000000) != 0 &&
                    (negResult & 0b100000) != 0 &&
                    (negResult & 0b10000) != 0 &&
                    (negResult & 0b1000) != 0 &&
                    (negResult & 0b100) != 0 &&
                    (negResult & 0b10) != 0 &&
                    (negResult & 0b1) != 0) {
                status |= STATUS_MASK_ZERO;
            }
            vm.writeData(status, REG_STATUS);
            vm.writeData(result, REG_A);
        }

        @Override
        public String name() {
            return "DEC";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_AND = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte b = vm.readData(REG_B);
            byte c = vm.readData(REG_C);
            byte result = (byte) ((b & c) & 0xff);
            byte status = vm.readData(REG_STATUS);
            // Clear V N Z
            status &= ~(STATUS_MASK_OVERFLOW | STATUS_MASK_NEGATIVE | STATUS_MASK_ZERO);
            if ((result & 0x80) != 0) {
                status |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                status |= STATUS_MASK_ZERO;
            }
            vm.writeData(status, REG_A);
            vm.writeData(result, REG_STATUS);
        }

        @Override
        public String name() {
            return "AND";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_OR = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte b = vm.readData(REG_B);
            byte c = vm.readData(REG_C);
            byte result = (byte) ((b | c) & 0xff);
            byte status = vm.readData(REG_STATUS);
            // Clear V N Z
            status &= ~(STATUS_MASK_OVERFLOW | STATUS_MASK_NEGATIVE | STATUS_MASK_ZERO);
            if ((result & 0x80) != 0) {
                status |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                status |= STATUS_MASK_ZERO;
            }
            vm.writeData(status, REG_A);
            vm.writeData(result, REG_STATUS);
        }

        @Override
        public String name() {
            return "OR";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_XOR = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte b = vm.readData(REG_B);
            byte c = vm.readData(REG_C);
            byte result = (byte) (Byte.toUnsignedInt(b) ^ Byte.toUnsignedInt(c));
            byte status = vm.readData(REG_STATUS);
            // Clear V N Z
            status &= ~(STATUS_MASK_OVERFLOW | STATUS_MASK_NEGATIVE | STATUS_MASK_ZERO);
            if ((result & 0x80) != 0) {
                status |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                status |= STATUS_MASK_ZERO;
            }
            vm.writeData(status, REG_A);
            vm.writeData(result, REG_STATUS);
        }

        @Override
        public String name() {
            return "XOR";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_NOT = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_A);
            byte result = (byte) ((~a) & 0xff);
            byte status = vm.readData(REG_STATUS);
            // Clear V N Z
            status &= ~(STATUS_MASK_OVERFLOW | STATUS_MASK_NEGATIVE | STATUS_MASK_ZERO);
            if ((result & 0x80) != 0) {
                status |= STATUS_MASK_NEGATIVE;
            }
            if (result == 0) {
                status |= STATUS_MASK_ZERO;
            }
            vm.writeData(status, REG_A);
            vm.writeData(result, REG_STATUS);
        }

        @Override
        public String name() {
            return "NOT";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_READ_AT = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            int b = Byte.toUnsignedInt(vm.readData(REG_B)); // lsb
            int c = Byte.toUnsignedInt(vm.readData(REG_C)); // msb
            int address = b | (c << 8);
            byte result = vm.readData(address);
            vm.writeData(result, REG_A);
        }

        @Override
        public String name() {
            return "READ_AT";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };
    public static final Instruction INST_WRITE_AT = new Instruction() {
        @Override
        public void execute(PlumVM vm) {
            byte a = vm.readData(REG_A);
            int b = Byte.toUnsignedInt(vm.readData(REG_B)); // lsb
            int c = Byte.toUnsignedInt(vm.readData(REG_C)); // msb
            int address = b | (c << 8);
            vm.writeData(a, address);
        }

        @Override
        public String name() {
            return "WRITE_AT";
        }

        @Override
        public int numArgs() {
            return 0;
        }
    };

    @Override
    public Optional<Instruction> getInstruction(byte op) {
        return switch (op) {
            case OP_LOAD_A -> Optional.of(INST_LOAD_A);
            case OP_LOAD_B -> Optional.of(INST_LOAD_B);
            case OP_LOAD_C -> Optional.of(INST_LOAD_C);
            case OP_LOAD_STATUS -> Optional.of(INST_LOAD_STATUS);
            case OP_ADD -> Optional.of(INST_ADD);
            case OP_SUB -> Optional.of(INST_SUB);
            case OP_JUMP -> Optional.of(INST_JUMP);
            case OP_RELATIVE_JUMP ->  Optional.of(INST_RELATIVE_JUMP);
            case OP_CALL -> Optional.of(INST_CALL);
            case OP_RELATIVE_CALL -> Optional.of(INST_RELATIVE_CALL);
            case OP_RELATIVE_JUMP_IF_ZERO -> Optional.of(INST_RELATIVE_JUMP_IF_ZERO);
            case OP_RELATIVE_JUMP_IF_OVERFLOW -> Optional.of(INST_RELATIVE_JUMP_IF_OVERFLOW);
            case OP_RELATIVE_JUMP_IF_NEGATIVE -> Optional.of(INST_RELATIVE_JUMP_IF_NEGATIVE);
            case OP_RELATIVE_JUMP_IF_CARRY -> Optional.of(INST_RELATIVE_JUMP_IF_CARRY);
            case OP_RETURN -> Optional.of(INST_RETURN);
            case OP_PUSH -> Optional.of(INST_PUSH);
            case OP_POP -> Optional.of(INST_POP);
            case OP_WRITE_A -> Optional.of(INST_WRITE_A);
            case OP_WRITE_B -> Optional.of(INST_WRITE_B);
            case OP_WRITE_C -> Optional.of(INST_WRITE_C);
            case OP_WRITE_STATUS -> Optional.of(INST_WRITE_STATUS);
            case OP_WRITE_AT -> Optional.of(INST_WRITE_AT);
            case OP_READ_A -> Optional.of(INST_READ_A);
            case OP_READ_B -> Optional.of(INST_READ_B);
            case OP_READ_C -> Optional.of(INST_READ_C);
            case OP_READ_STATUS -> Optional.of(INST_READ_STATUS);
            case OP_READ_AT -> Optional.of(INST_READ_AT);
            case OP_SWAP_A_B -> Optional.of(INST_SWAP_A_B);
            case OP_SWAP_A_C -> Optional.of(INST_SWAP_A_C);
            case OP_SWAP_A_STATUS -> Optional.of(INST_SWAP_A_STATUS);
            case OP_CMP -> Optional.of(INST_CMP);
            case OP_INC -> Optional.of(INST_INC);
            case OP_DEC -> Optional.of(INST_DEC);
            case OP_AND -> Optional.of(INST_AND);
            case OP_OR -> Optional.of(INST_OR);
            case OP_XOR -> Optional.of(INST_XOR);
            case OP_NOT -> Optional.of(INST_NOT);
            default -> Optional.empty();
        };
    }

    public String nextByteOpName(PlumVM vm) {
        byte nextByte = vm.getProgramMemory().rawRead(vm.getPc());
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("0x%x", nextByte));
        Optional<Instruction> next = getInstruction(nextByte);
        if (next.isEmpty()) {
            return sb.toString();
        }
        sb.append(" (");
        Instruction instruction = next.get();
        sb.append(instruction.name());
        for (int i = 0; i < instruction.numArgs(); i ++) {
            int index = vm.getPc() + i + 1;
            if (vm.getProgramMemory().outOfBounds(index)) {
                sb.append(" ERR");
                continue;
            }
            byte arg = vm.getProgramMemory().rawRead(index);
            sb.append(String.format(" 0x%x", arg));
        }
        return sb.append(")").toString();
    }

    public static String getReadableStatus(PlumVM vm) {
        StringBuilder sb = new StringBuilder();
        byte status = vm.getDataMemory().rawRead(REG_STATUS);
        sb.append(((status & STATUS_MASK_INTERRUPT) != 0) ? "I" : "-");
        sb.append(((status & STATUS_MASK_OVERFLOW) != 0) ? "O" : "-");
        sb.append(((status & STATUS_MASK_ZERO) != 0) ? "Z" : "-");
        sb.append(((status & STATUS_MASK_NEGATIVE) != 0) ? "N" : "-");
        sb.append(((status & STATUS_MASK_CARRY) != 0) ? "C" : "-");
        return sb.toString();
    }

    @Override
    public void displayInfo(PlumVM vm) {
        System.out.println("VM {");
        System.out.printf("\tCLOCKS:   %d\n", vm.getClocks());
        System.out.printf("\tPC:       0x%x\n", vm.getPc());
        System.out.printf("\tSP:       0x%x\n", vm.getSp());
        System.out.printf("\tnextByte: %s\n", nextByteOpName(vm));
        System.out.printf("\tREG_A:    0x%x\n", vm.getDataMemory().rawRead(REG_A));
        System.out.printf("\tREG_B:    0x%x\n", vm.getDataMemory().rawRead(REG_B));
        System.out.printf("\tREG_C:    0x%x\n", vm.getDataMemory().rawRead(REG_C));
        System.out.printf("\tREG_STATUS: %s (%s)\n", String.format("%5s", vm.getDataMemory().rawRead(REG_STATUS)).replaceAll(" ", "0"),
                            getReadableStatus(vm));
        System.out.println("}");
    }

    @Override
    public int pcStartVector() {
        return 0;
    }
}
