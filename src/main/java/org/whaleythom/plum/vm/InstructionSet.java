package org.whaleythom.plum.vm;

import java.util.Optional;

public interface InstructionSet {
    Optional<Instruction> getInstruction(byte op);

    void displayInfo(PlumVM vm);

    @SuppressWarnings("SameReturnValue")
    int pcStartVector();
}
