package org.whaleythom.plum.vm;

public interface Instruction {
    void execute(PlumVM vm);

    String name();

    int numArgs();
}
