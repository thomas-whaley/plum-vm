package org.whaleythom.plum.vm;

import org.whaleythom.plum.vm.memory.Memory;

import java.util.Optional;

public class PlumVM {
    private final Memory programMemory;
    private final Memory dataMemory;
    private final Display display;
    private int clocks = 0;

    private int pc;

    private int sp;

    private final InstructionSet instructionSet;

    public PlumVM(InstructionSet instructionSet, Memory programMemory, Memory dataMemory, Display display) {
        this.instructionSet = instructionSet;
        this.programMemory = programMemory;
        this.dataMemory = dataMemory;
        this.display = display;
        pc = instructionSet.pcStartVector();
        sp = display.videoMemoryStart(this) - 1;
    }

    @SuppressWarnings("unused")
    public void writeProgram(byte data, int index) {
        clocks++;
        programMemory.write(data, index);
    }

    @SuppressWarnings("unused")
    public void writeProgram(byte[] data, int start) {
        clocks += data.length;
        programMemory.write(data, start);
    }

    @SuppressWarnings("unused")
    public void writeWordProgram(int data, int index) {
        clocks += 2;
        programMemory.writeWord(data, index);
    }

    public byte readProgram(int index) {
        clocks++;
        return programMemory.read(index);
    }

    @SuppressWarnings("unused")
    public byte[] readProgram(int startIndex, int size) {
        clocks += size;
        return programMemory.read(startIndex, size);
    }

    public int readWordProgram(int index) {
        clocks += 2;
        return programMemory.readWord(index);
    }

    public Memory getProgramMemory() {
        return programMemory;
    }

    public void writeData(byte data, int index) {
        clocks++;
        dataMemory.write(data, index);
    }

    @SuppressWarnings("unused")
    public void writeData(byte[] data, int start) {
        clocks += data.length;
        dataMemory.write(data, start);
    }

    public void writeWordData(int data, int index) {
        clocks += 2;
        dataMemory.writeWord(data, index);
    }

    public byte readData(int index) {
        clocks++;
        return dataMemory.read(index);
    }

    @SuppressWarnings("unused")
    public byte[] readData(int startIndex, int size) {
        clocks += size;
        return dataMemory.read(startIndex, size);
    }

    public int readWordData(int index) {
        clocks += 2;
        return dataMemory.readWord(index);
    }

    public Memory getDataMemory() {
        return dataMemory;
    }

    public byte getProgramAtPc() {
        byte data = readProgram(pc);
        pc++;
        return data;
    }

    public int getProgramWordAtPc() {
        int word = readWordProgram(pc);
        pc += 2;
        return word;
    }

    @SuppressWarnings("unused")
    public byte popSp() {
        sp++;
        return readData(sp);
    }

    public int popWordSp() {
        sp ++;
        int word = readWordData(sp);
        sp ++;
        return word;
    }

    @SuppressWarnings("unused")
    public void pushSp(byte data) {
        writeData(data, sp);
        sp--;
    }

    public void pushWordSp(int data) {
        sp --;
        writeWordData(data, sp);
        sp --;
    }

    public int getPc() {
        return pc;
    }

    public int getSp() {
        return sp;
    }

    public void setSp(int newSp) {
        if (dataMemory.outOfBounds(newSp)) {
            throw new IllegalArgumentException(String.format("Invalid SP (0x%x). Max index 0x%x", newSp, programMemory.maxIndex()));
        }
        sp = newSp;
    }

    public void setPc(int newPc) {
        if (programMemory.outOfBounds(newPc)) {
            throw new IllegalArgumentException(String.format("Invalid PC (0x%x). Max index 0x%x", newPc, programMemory.maxIndex()));
        }
        pc = newPc;
    }

    public int getClocks() {
        return clocks;
    }

    public void clock() {
        byte op = getProgramAtPc();
        Optional<Instruction> next = instructionSet.getInstruction(op);
        if (next.isPresent()) {
            Instruction instruction = next.get();
            instruction.execute(this);
        }
        display.update(this);
    }

    public InstructionSet getInstructionSet() { return instructionSet; }

    public void displayInfo() {
        instructionSet.displayInfo(this);
    }
}
