import org.junit.jupiter.api.Test;
import org.whaleythom.plum.vm.InstructionSet;
import org.whaleythom.plum.vm.PlumInstructionSet;
import org.whaleythom.plum.vm.PlumVM;
import org.whaleythom.plum.vm.memory.ArrayMemory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InstructionSetTest {
    private static final InstructionSet instructionSet = new PlumInstructionSet();

    private static PlumVM DefaultFactory() {
        return new PlumVM(instructionSet, new ArrayMemory(), new ArrayMemory());
    }

    /**
     * Write to program memory so that the clock cycles don't get interfered with
     */
    private static void writeProgram(byte value, int index, PlumVM vm) {
        vm.getProgramMemory().write(value, index);
    }

    @SuppressWarnings("unused")
    public static byte readProgram(int index, PlumVM vm) {
        return vm.getProgramMemory().read(index);
    }

    private static void writeData(byte value, int index, PlumVM vm) {
        vm.getDataMemory().write(value, index);
    }

    public static byte readData(int index, PlumVM vm) {
        return vm.getDataMemory().read(index);
    }

    @Test
    public void testLoadA() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_LOAD_A, 0, vm);
        writeProgram((byte) 0xff, 1, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_A, vm));
        vm.clock();
        assertEquals((byte) 0xff, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testLoadB() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_LOAD_B,  0, vm);
        writeProgram((byte) 0xff,  1, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_B, vm));
        vm.clock();
        assertEquals((byte) 0xff, readData(PlumInstructionSet.REG_B, vm));
    }

    @Test
    public void testLoadC() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_LOAD_C,  0, vm);
        writeProgram((byte) 0xff,  1, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_C, vm));
        vm.clock();
        assertEquals((byte) 0xff, readData(PlumInstructionSet.REG_C, vm));
    }

    @Test
    public void testLoadStatus() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_LOAD_STATUS,  0, vm);
        writeProgram((byte) 0xff,  1, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm));
        vm.clock();
        assertEquals((byte) 0xff, readData(PlumInstructionSet.REG_STATUS, vm));
    }

    @Test
    public void testAddSimple() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 1,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 2,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_A, vm));
        vm.clock();
        assertEquals(3, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testAddByteOverflow() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x11,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_A, vm));
        vm.clock();
        assertEquals(1, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testAddStatusCarry() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_CARRY, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
    }

    @Test
    public void testAddStatusNotCarry() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x5,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
    }


    @Test
    public void testAddStatusOverflow1() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0x80,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x80,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_OVERFLOW, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
    }

    @Test
    public void testAddStatusOverflow2() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0x7f,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x7f,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_OVERFLOW, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
    }

    @Test
    public void testAddStatusNotOverflow() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x1,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
    }


    @Test
    public void testAddStatusNegative() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_NEGATIVE, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
    }

    @Test
    public void testAddStatusNotNegative() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0x70,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xf,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
    }


    @Test
    public void testAddStatusZero1() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_ZERO, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
    }

    @Test
    public void testAddStatusZero2() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xff,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x1,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_ZERO, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
    }

    @Test
    public void testAddStatusNotZero() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_ADD,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xf,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
    }
    @Test
    public void testSubSimple() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 3,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 2,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_A, vm));
        vm.clock();
        assertEquals(1, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testSubByteOverflow() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x1,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_A, vm));
        vm.clock();
        assertEquals((byte)0xff, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testSubStatusCarry() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_CARRY, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
    }

    @Test
    public void testSubStatusNotCarry() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x80,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x1,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                                        PlumInstructionSet.STATUS_MASK_CARRY);
    }


    @Test
    public void testSubStatusOverflow1() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0xfe,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x7f,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_OVERFLOW, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
    }

    @Test
    public void testSubStatusOverflow2() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x81,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x7f,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_OVERFLOW, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
    }

    @Test
    public void testSubStatusNotOverflow() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0xff,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x7f,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_OVERFLOW);
    }


    @Test
    public void testSubStatusNegative() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xf,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_NEGATIVE, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
    }

    @Test
    public void testSubStatusNotNegative() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x70,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xf,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
    }


    @Test
    public void testSubStatusZero() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        vm.clock();
        assertEquals(PlumInstructionSet.STATUS_MASK_ZERO, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
    }

    @Test
    public void testSubStatusNotZero() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_SUB,  0, vm);
        writeData((byte) 0xf0,  PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xf,  PlumInstructionSet.REG_C, vm);
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        vm.clock();
        assertEquals(0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
    }

    @Test
    public void testJump() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_JUMP, 0, vm);
        writeProgram((byte) 0xf0, 1, vm);
        writeProgram((byte) 0xf0, 2, vm);
        assertEquals(0, vm.getPc());
        vm.clock();
        assertEquals(0xf0f0, vm.getPc());
    }

    @Test
    public void testJumpLittleEndian() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_JUMP, 0, vm);
        writeProgram((byte) 0x00, 1, vm);
        writeProgram((byte) 0xf0, 2, vm);
        assertEquals(0, vm.getPc());
        vm.clock();
        assertEquals(0xf000, vm.getPc());
    }

    @Test
    public void testCall() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_CALL, 0, vm);
        writeProgram((byte) 0xf0, 1, vm);
        writeProgram((byte) 0xf0, 2, vm);
        assertEquals(0, vm.getPc());
        assertEquals(0xffff, vm.getSp());
        vm.clock();
        assertEquals(0xf0f0, vm.getPc());
        assertEquals(0xfffd, vm.getSp());
        assertEquals(0, readData(vm.getSp(), vm));
        assertEquals(0, readData(vm.getSp(), vm));
    }

    @Test
    public void testCallJumpLittleEndian() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_CALL, 0, vm);
        writeProgram((byte) 0xff, 1, vm);
        writeProgram((byte) 0xf0, 2, vm);
        assertEquals(0, vm.getPc());
        assertEquals(0xffff, vm.getSp());
        vm.clock();
        assertEquals(0xf0ff, vm.getPc());
        assertEquals(0xfffd, vm.getSp());
        assertEquals(0, readData(vm.getSp(), vm));
        assertEquals(3, readData(vm.getSp() + 1, vm));
    }

    @Test
    public void testCallPushLittleEndian() {
        PlumVM vm = DefaultFactory();
        vm.setPc(0x0001);
        writeProgram(PlumInstructionSet.OP_CALL, 1, vm);
        writeProgram((byte) 0xf0, 2, vm);
        writeProgram((byte) 0xf0, 3, vm);
        assertEquals(1, vm.getPc());
        assertEquals(0xffff, vm.getSp());
        vm.clock();
        assertEquals(0xf0f0, vm.getPc());
        assertEquals(0xfffd, vm.getSp());
        assertEquals(4, readData(vm.getSp() + 1, vm));
        assertEquals(0, readData(vm.getSp() + 2, vm));
    }

    @Test
    public void testReturn() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_RETURN, 0, vm);
        vm.setSp(0xfffd);
        writeData((byte) 0xf0, vm.getSp() + 1, vm);
        writeData((byte) 0xf0, vm.getSp() + 2, vm);
        assertEquals(0xfffd, vm.getSp());
        assertEquals(0x0, vm.getPc());
        vm.clock();
        assertEquals(0xffff, vm.getSp());
        assertEquals(0xf0f0, vm.getPc());

    }

    @Test
    public void testPush() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_PUSH, 0, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_A, vm);
        assertEquals(0xffff, vm.getSp());
        assertEquals((byte) 0, readData(vm.getSp(), vm));
        vm.clock();
        assertEquals(0xfffe, vm.getSp());
        assertEquals((byte) 0xff, readData(vm.getSp() + 1, vm));
    }

    @Test
    public void testPop() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_POP, 0, vm);
        writeData((byte) 0xff, vm.getSp(), vm);
        vm.setSp(vm.getSp() - 1);
        assertEquals(0xfffe, vm.getSp());
        assertEquals((byte) 0, readData(vm.getSp(), vm));
        vm.clock();
        assertEquals(0xffff, vm.getSp());
        assertEquals((byte) 0xff, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testWriteA() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_WRITE_A, 0, vm);
        writeProgram((byte) 0xff, 1, vm);
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_A, vm));
        writeData((byte) 0xff, 0xe0, vm);
        vm.clock();

    }

    @Test
    public void testCmpDoesNotAffectRegisters() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_CMP, 0, vm);
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_A, vm));
        writeData((byte) 0xff, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_A, vm));
    }

    @Test
    public void testCmpCarry() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_CARRY);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_CARRY);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertNotEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_CARRY);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertNotEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_CARRY);
    }

    @Test
    public void testCmpNegative() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertNotEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertNotEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_NEGATIVE);
    }

    @Test
    public void testCmpZero() {
        PlumVM vm = DefaultFactory();
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertNotEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0xff, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x00, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
        writeProgram(PlumInstructionSet.OP_CMP, vm.getPc(), vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_B, vm);
        writeData((byte) 0x0f, PlumInstructionSet.REG_C, vm);
        vm.clock();
        assertNotEquals((byte) 0, readData(PlumInstructionSet.REG_STATUS, vm) &
                PlumInstructionSet.STATUS_MASK_ZERO);
    }
}
